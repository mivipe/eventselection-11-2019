FROM atlas/analysisbase:21.2.96
ADD . /analysis/src
WORKDIR /analysis/build
RUN source /home/atlas/release_setup.sh && sudo chown -R atlas /analysis &&  cmake ../src && make -j4
